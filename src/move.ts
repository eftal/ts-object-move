type List = { id: string; name: string; files: File[] }[];
type File = { id: string; name: string };

export default function move(list: List, source: string, destination: string): List {
  function findItemNested(itemId: string) {
    for (let i = 0; i < list.length; i += 1) {
      const rootPath = list[i];
      if (rootPath.id === itemId) return rootPath;
      const file = rootPath.files.find((v) => v.id === itemId);
      if (file) return file;
    }
    return null;
  }

  const sourceObject = findItemNested(source);
  const destinationObject = findItemNested(destination);
  if (!sourceObject) {
    throw new Error('Source file does not found :(');
  }

  if (!destinationObject) {
    throw new Error('Destination folder does not found :(');
  }

  if (Object.keys(sourceObject).includes('files')) {
    throw new Error('You cannot move a folder');
  }
  if (!Object.keys(destinationObject).includes('files'))
    throw new Error('You cannot specify a file as the destination');

  list.forEach((folder) => {
    const fileIndex = folder.files.indexOf(sourceObject);
    if (folder === destinationObject) folder.files.push(sourceObject);
    if (fileIndex > -1) folder.files.splice(fileIndex, 1);
  });

  return list;
}
